variable "scaleway_region" {
  description = "Scaleway region"
  type        = string
}

variable "scaleway_zone" {
  description = "Scaleway zone"
  type        = string
}

variable "scaleway_kapsule_cluster_name" {
  description = "Name of kapsule cluster"
  type        = string
}

variable "scaleway_kapsule_cluster_project_id" {
  description = "Project id in which the cluster must be created"
  type        = string
}

variable "scaleway_kapsule_cluster_cni" {
  description = "CNI of kapsule cluster"
  type        = string
  default     = "cilium"
}

variable "scaleway_kapsule_cluster_node_type" {
  description = "Node instance type of kapsule cluster"
  type        = string
  default     = "DEV1-M"
}

variable "scaleway_kapsule_cluster_pool_size" {
  description = "Amount of worker in the pool of kapsule cluster"
  type        = number
  default     = 2
}

variable "scaleway_kapsule_cluster_pool_size_min" {
  description = "Minimum number of workers in the pool of kapsule cluster"
  type        = number
  default     = 2
}

variable "scaleway_kapsule_cluster_pool_size_max" {
  description = "Maximum number of workers in the pool of kapsule cluster"
  type        = number
  default     = 5
}
